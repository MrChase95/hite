#Imports
import math as m

#Definindo Objeto

#FUNÇÕES

def getRadius(body, inumPl):
    

    for i in range(1, inumPl):
        body[i].dRadius = m.sqrt(body[i].dDepth)*body[0].dRadius



def getMass(body, inumPl):
    body[0].dMass = m.pow(10,body[0].dLog_g) * body[0].dRadius * body[0].dRadius/(const.GRAVCONST/100)

    for i in range(1, inumPl):
        if body[i].dRadius/const.REARTH <1:
            body[i].dMass = m.pow(body[i].dRadius/const.REARTH, 3.268)* const.MEARTH
        elif body[i].dRadius/const.REARTH >= 1 and body[i].dRadius/const.REARTH < 2.5:
            body[i].dMass = m.pow(body[i].dRadius/const.REARTH, 3.65)* const.MEARTH
        else: 
            body[i].dMass = (4*const.PI/3)*1000*m.pow(body[i].dRadius, 3)

def getGrav(body):
    body[0].dGrav = const.GRAVCONST * body[0].dMass/ (m.pow(body[0].dRadius,2))

def getSemis(body, inumPl):
    for i in range(1, inumPl):
        #DUVIDA: BIGG* <- O que seria isso?

        body[i].dSemi = m.pow((body[0].dMass + body[i].dMass)/(4*const.PI* const.PI)*body[i].dPeriod*body[i].dPeriod,(1/3))

def getTDAs(body, inumPl):
    for i in range(1, inumPl):
        body[i].dCircDur = m.sqrt((1 - body[i].dImpact*body[i].dImpact)*m.pow((body[0].dRadius*body[i].dRadius),2))*body[i].dPeriod/(const.PI*body[i].dSemi)
        body[i].dTDA = body[i].dDuration/body[i].dCircDur

def getEmins(body, inumPl):
    for i in range(1, inumPl):
        body[i].dEmin = m.fabs((body[i].dTDA * body[i].dTDA - 1)/(body[i].dTDA + 1))

def dHillEmax(gamma, lambdaa, mu, zeta):
    gamma = gamma[2]
    mu = mu[2]

    arg1 = 1 + m.pow(3, (4, 3) * (mu[0] * mu[1]/m.pow(zeta), (4/3)))
    arg2 = m.pow(zeta, 3)/(mu[0] * mu[1]/ (lambdaa * lambdaa))
    arg3 = mu[1] * gamma[1] * lambdaa

    gamma[0] = mu[1]*gamma[1]*lambdaa

    if m.fabs(gamma[0] > 1):
        return -1 
    else:
        return m.sqrt(1 - gamma[2]*gamma[0])

def getEmax(body, inumPl):
    mu,gamma = [],[]
    zeta = 0.0
    lambdaa = 0.0
    dTotMass = 0.0
    emax = 0.0

    if numPl > 1:
        dTotMass = body[0].dMass + body[1].dMass + body[2].dMass
        mu[0] = body[1].dMass/body[0].dMass
        mu[1] = body[2].dMass/body[0].dMass
        zeta = mu[0] + mu[1]
        gamma[1] = m.sqrt(1-body[2].dEmin*body[2].dEmin)
        if body[2].iPos == 2:
            lambdaa = m.sqrt(body[1].dSemi/body[2].dSemi)
        else: 
            lambdaa = m.sqrt(body[2].dSemi/body[1].dSemi)

        body[1].dEmax = dHillEmax(gamma, lambdaa, mu, zeta)

        if inumPl == 3:
            mu[1] = body[3].dMass/body[0].dMass
            zeta = mu[0] + mu[1]
            gamma[1] = m.sqrt(1-body[3].dEmin*body[3].dEmin)
            if body[3].iPos == 2:
                lambdaa = m.sqrt(body[1].dSemi/body[3].dSemi)
            else:
                lambdaa = m.sqrt(body[3].dSemi/body[1].dSemi)

        emax = dHillEmax(gamma, lambdaa,mu,zeta)

        if emax < body[1].dEmax:
            body[1].dEmax = emax
        else:
            body[1].dEmax = EMAX
def getLuminosity(body):
    #Pedir ajuda
    body.dLuminosity = 4*PI*body.dRadius*body.dRadius*SB*pow(body.dTemp,4)

def getFmax(body):
    pstar = 0.0

    pstar = PREF*m.exp(LH2O/(RGAS*TREF))
    body.dFmax = A*SB*pow(LH2O/(RGAS*log(pstar*sqrt(K0/(2*PLINE*body.dGrav)))),4);

def getInstellation(body):
    body[1].dSeff = body[0].dLuminosity/(4*PI*body[1].dSemi*body[1].dSemi)


def pofe(e):
    return 0.1619 - 0.5352 * e + 0.6358*e*e - 0.2557*m.pow(e,3)

def procky(radius):
    if radius/REARTH > 2.5:
        return 0
    if radius/REARTH > 1.5:
        return 2.5 - radius/REARTH
    
    return 1

def scanEccAlb(body, dEmin, dEmax):
    if dEmax < 0 or dEmin > dEmax:
        return 0

    de = 0.01
    da = 0.01

    flux0 = body[0].dLuminosity/(16*PI*body[1].dSemi*body[1].dSemi)

    for a in range(ALBMIN,ALBMAX,da):
        for e in range(dEmin, dEmax, de):
            flux = flux0 * (1-a)/ m.sqrt(1-e*e)
            dTot += pofe(e)
            if flux < body[1].dFmax and flux > MINFLUX:
                dHabFact += pofe(e)
            if flux > body[1].dFmax:
                bIHZ = True
            if flux < MINFLUX:
                bOHZ = True

    if bIHZ == False and bOHZ == False:
        body[1].iConstraint = 0
    if bIHZ:
        body[1].iConstraint = 1
    if bOHZ:
        body[1].iConstraint = 2
    if bIHZ and bOHZ: 
        body[1].iConstraint = 3

    return (dHabFact/dTot)*body[1].dProcky

def HabFact(body, options):
    

    getRadius(body, options)
    getMass(body, options)
    getGrav(body[1])
    getSemis(body, options)
    getTDAs(body, options)
    getEmins(body, options)
    getEmax(body, options)
    getFmax(body[1])
    getLuminosity(body[0])
    getInstellation(body)
    body[1].dProcky = procky(body[1].dRadius)

    body.dHabFact = scanEccAlb(body, const.EMIN, const.EMAX)

    if body[1].dEmax < const.EMAX:
        body[1].dHabHactEcc = scanEccAlb(body[1], body[1].dEmin, body[1].dEmax)
    else:
        body[1].dHabHactEcc = scanEccAlb(body[1], body[1].dEmin, const.EMAX)


#CLASSES BASES
class const():
    def __init__(self):
        self.PI = 3.1415926535
        self.GRAVCONST = 6.67428 * (10 ** -11)
        self.A = 0.7344
        self.SB = 5.670373 * (10 ** -8)
        self.LH2O = 2.425 * (10 ** 6)
        self.RGAS = 461.5
        self.PLINE = 1 * (10 ** 4)
        self.PREF = 610.616
        self.TREF = 273.13
        self.K0 = 0.055
        self.MEARTH = 5.972186 * (10 ** 24)
        self.REARTH = 6378100
        self.S0 = 1362
        self.MSUN = 1.988416 * (10 ** 30)
        self.RSUN = 6.957 * (10 ** 8)
        self.LSUN = 3.828 * (10 ** 26)
        self.AUM = 1.49598 * (10 ** 11)
        self.HRSEC = 3600
        self.DEYSEC = 86400
        self.PPM = 1 * (10 ** -6)

        self.EMIN = 0
        self.EMAX = 0.8
        self.ALBMIN = 0.05
        self.ALBMAX = 0.8
        self.MINFLUX = 67

        self.LINE = 128
        self.OPTLEN = 24
        self.MAXLINES = 128

class body():
    def __init__(self):
        self.iPos = None
        self.iConstraint = None

        self.dDepth = 614.1 # vem do usuario
        self.dImpact = 0.22 # vem do usuario
        self.dDuration = 5.621 # vem do usuario
        self.dPeriod = 112.3053 # vem do usuario

        self.dRadius = 0.6 # vem do usuario
        self.dMass = None 
        self.dGrav = None
        self.dEmin = None
        self.dEmax = None
        self.dSemi = None
        self.dTDA = None
        self.dCircDur = None
        self.dProcky = None
        self.dSeff = None
        self.dHabFact = None
        self.dHabHactEcc = None

        self.dTemp = 4402 # vem do usuario
        self.dLuminosity = None 
        self.dLog_g = 4.67 # vem do usuario

        


def main():
    
    numPl = 1
    if numPl == 1:
        p = [body()]
    HabFact(p, 1)

main()